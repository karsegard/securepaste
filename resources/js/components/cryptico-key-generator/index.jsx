import React,{useEffect,useState} from 'react';
import ReactDOM from "react-dom";
import reactToWebComponent from "react-to-webcomponent";
import PropTypes from 'prop-types';
import * as openpgp from 'openpgp';
const generateKey = async (passphrase='') => {
    const { privateKey, publicKey, revocationCertificate } = await openpgp.generateKey({
        type: 'ecc', // Type of the key, defaults to ECC
        curve: 'curve25519', // ECC curve name, defaults to curve25519
        userIDs: [{ name: 'Jon Smith', email: 'jon@example.com' }], // you can pass multiple user IDs
        passphrase, // protects the private key
        format: 'armored' // output key format, defaults to 'armored' (other options: 'binary' or 'object')
    });

   
    return {privateKey,publicKey};
}



const GeneratedKey = ({bits,passphrase}) =>{

    const [privateKey, setPrivateKey]= useState('');
    const [publicKey, setPublicKey]= useState('');
    
    useEffect(()=>{
        generateKey(passphrase).then(({privateKey:_privateKey,publicKey:_publicKey})=>{
            setPrivateKey(_privateKey);
            setPublicKey(_publicKey);
        });
    },[bits,passphrase])


    return (
        <>
            <textarea className="privatekey" defaultValue={privateKey}></textarea>
            <textarea className="publickey" defaultValue={publicKey}></textarea>
        </>
    )
}


const Index = ({bits=2048}) => {
    const [passphrase,setPassphrase]= useState('');
    return (
        <>
            <input type="password" placeholder="Choisissez un mot de passe" value={passphrase} onChange={e=>setPassphrase(e.target.value)}/>
            <GeneratedKey bits={bits} passphrase={passphrase}/>
        </>
    )};

Index.propTypes = {
    bits: PropTypes.string,
    passphrase: PropTypes.string,
};

customElements.define("cryptico-key-generator", reactToWebComponent(Index, React, ReactDOM));