<?php

namespace KDA\Securepaste\Database\Factories;

use KDA\Securepaste\Models\Paste;
use Illuminate\Database\Eloquent\Factories\Factory;

class PasteFactory extends Factory
{
    protected $model = Paste::class;

    public function definition()
    {
        return [
            //
        ];
    }
}
