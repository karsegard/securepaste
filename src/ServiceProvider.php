<?php

namespace KDA\Example;

use KDA\Laravel\PackageServiceProvider;
//use Illuminate\Support\Facades\Blade;

class ServiceProvider extends PackageServiceProvider
{

    //use \KDA\Laravel\Traits\HasAssets; 
    use \KDA\Laravel\Traits\HasCommands;
    //use \KDA\Laravel\Traits\HasComponents;
   // use \KDA\Laravel\Traits\HasComponentNamespaces;

    //use \KDA\Laravel\Traits\HasConfig;
    //use \KDA\Laravel\Traits\HasDumps;
    //use \KDA\Laravel\Traits\HasDynamicSidebar;
    //use \KDA\Laravel\Traits\HasHelper;
    use \KDA\Laravel\Traits\HasLoadableMigration;
    //use \KDA\Laravel\Traits\HasMigration;
    //use \KDA\Laravel\Traits\HasObservers;
    //use \KDA\Laravel\Traits\HasProviders;
    //use \KDA\Laravel\Traits\HasPublicAssets;
    //use \KDA\Laravel\Traits\HasRoutes;
    //use \KDA\Laravel\Traits\HasTranslations;
    //use \KDA\Laravel\Traits\HasVendorViews;
    //use \KDA\Laravel\Traits\HasViews;
    //use \KDA\Laravel\Traits\HasYamlConfig;
    //use \KDA\Laravel\Traits\ProvidesBlueprint;


    /* This is mandatory to avoid problem with the package path */
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }


    /**
     * trait \KDA\Laravel\Traits\HasAssets; 
     * 
     * registers assets to be publishables
     */
    // protected $assetsDir = 'assets';

    //-------------------------------------------

    /**
     * use \KDA\Laravel\Traits\HasCommands
     * registers package commands
     */

    protected $_commands = [
        Commands\InstallCommand::class
    ];

    //-------------------------------------------


    /**
     * trait \KDA\Laravel\Traits\HasComponents;
     */

    /*protected $components = [
        'sc-render' => View\Components\Render::class,
    ];*/


     //-------------------------------------------


    //-------------------------------------------


    /**
     * trait \KDA\Laravel\Traits\HasComponentNamespaces;
     */

    /*protected $componentNamespaces = [
        'View\\Components' => 'namespace'
    ];*/
    /*protected $anonymousComponentNamespaces = [
        'hello.world.components' => 'namespace'
    ];*/

     //-------------------------------------------




    /**
     *    registers config file as 
     *      file_relative_to_config_dir => namespace
     */
    // protected $configDir='config';
    /*protected $configs = [
        'kda/example.php'  => 'kda.example'
    ];*/

    //-------------------------------------------

    /**
     * trait: use \KDA\Laravel\Traits\HasDumps
     *  put the tables that should be included in a dump
     *  requires: fdt2k/laravel-database-dump        
     */
    //protected $dumps = [];

    //-------------------------------------------

    /**
     * trait \KDA\Laravel\Traits\HasDynamicSidebar
     * 
     * requires: fdt2k/backpack-dynamic-sidebar
     * 
     * registers dynamic sidebars as 
     *  [
     *      route_name => Translation Label
     *  ]
     */
    /*protected $sidebars = [
        [
            'label' => 'Navigation',
            'route' => 'scmnavigation',
            'behavior' => 'hide',
            'icon' => 'la-route'
        ]
    ];*/

    //-------------------------------------------

    /**
     * trait \KDA\Laravel\Traits\HasHelper
     * registers helpers files
     */
    // protected $helperDir = 'helpers';

    //-------------------------------------------

    /**
     * trait \KDA\Laravel\Traits\HasLoadableMigration
     * registers loadable and not published migrations
     * 
     */
    // protected $migrationDir = 'database/migrations';

    //-------------------------------------------

    /** trait use \KDA\Laravel\Traits\HasMigration
     *  registers publishable migrations
     * 
     */
    // protected $migrationDir = 'database/migrations';


    //------------------------------------------- 


    /**
     * trait  \KDA\Laravel\Traits\HasObservers
     * register model observers
     */
    //protected $observers = [
    //  \KDA\Example\Models\Example::class=> \KDA\Example\Observers\Example::class  
    //];


    //-------------------------------------------

    /**
     * trait  \KDA\Laravel\Traits\HasProviders
     * registers additionnal providers
     */

    //protected $additionnalProviders=[
    //  \KDA\Example\Providers\SomeProvider::class
    //];


    //-------------------------------------------


    /**
     * trait \KDA\Laravel\Traits\HasPublicAssets
     */

    // protected $publicDir = 'public';
    //-------------------------------------------


    /**
     * trait  \KDA\Laravel\Traits\HasRoutes
     * register routes, 
     * 
     */
    //protected $routesDir = 'routes';
    //protected $routes = [
    //    'backpack/structured_editor.php'
    //];
    //-------------------------------------------

    /**
     * trait  \KDA\Laravel\Traits\HasTranslations
     * register translations from directory (default: resources/lang)
     * 
     */
    //protected $translationsDir = '';
    //protected $publishTranslationsTo= '';
    //-------------------------------------------

    /**
     * trait \KDA\Laravel\Traits\HasVendorViews
     * register another set of views, used if we supercharge another package views so we don't mix up with
     * potential published view 
     */
    //protected $vendorViewsNamespace = 'kda-sidebar';
    //protected $publishVendorViewsTo = 'vendor/kda/backpack/sidebar';


    /**
     *  trait \KDA\Laravel\Traits\HasViews
     * 
     */
    //protected $viewNamespace = 'kda-sidebar';
    //protected $publishViewsTo = 'vendor/backpack/base';


    /**
     * trait \KDA\Laravel\Traits\HasYamlConfig
     * allow use of config as yaml file
     * use : pragmarx/yaml
     * same variables than HasConfig
     */



    /*

    */
    /*public function register()
    {
        parent::register();
    }*/


    /**
     * trait \KDA\Laravel\Traits\ProvidesBlueprint
     * Blueprint can be defined here
     */

    /**
     * called after the trait were registered
     */

    /*
    public function postRegister(){
        $this->blueprints =[
            'nestable'=> function($parent_id_name='parent_id',$constraint=NULL,$onDelete='restrict'){
                $table = $constraint ?? $this->table;
                $this->foreignId($parent_id_name)->nullable()->constrained($table)->onDelete($onDelete);
                $this->unsignedInteger('lft')->nullable()->default(0);
                $this->unsignedInteger('rgt')->nullable()->default(0);
                $this->unsignedInteger('depth')->nullable()->default(0);
            }
        ];
    }
*/


    /**
     * called after the trait were booted
     */
    /*
    protected function bootSelf(){

    //    $this->publishRoutes();

        Blade::directive('bind_values', function ($content) {
            return "<?php 
                foreach (\$content  as \$field => \$value) {
                    \$\$field = \$value;
                }
    
            ?>";
        });

    }*/

    /*
        Super charging the boot method
    */
    /*public function boot()
    {
        parent::boot();
    }*/
}
