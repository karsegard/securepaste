<?php

namespace KDA\Securepaste\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use KDA\Laravel\Models\Traits\HasUUIDPrimaryKey;
class Paste extends Model
{
    use HasFactory;
    use HasUUIDPrimaryKey;


    protected $fillable = [
        
    ];

    protected $appends = [
        
    ];

    protected $casts = [
       
    ];

   
    protected static function newFactory()
    {
        return  \KDA\Securepaste\Database\Factories\PasteFactory::new();
    }

}
